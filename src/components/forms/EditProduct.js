import React, { Fragment, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import Switch from "@material-ui/core/Switch";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import imgupload from "../../img/upload.png";
import { categories, manufacturers, warrantyTypes } from "../../utils/data";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { getProduct, editProduct } from "../../actions/products";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    color: "black",

    "& > *": {
      width: theme.spacing(200),
      height: theme.spacing(75),
    },
  },
  paper: {
    padding: "10px",
    margin: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
  },
  title: {
    flex: 1,
  },
  enable: {
    display: "flex",
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  name: {
    display: "flex",
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  productname: {
    width: theme.spacing(50),
  },
  formControl: {
    margin: theme.spacing(1),
    width: theme.spacing(50),
  },
  formControl2: {
    margin: theme.spacing(1),
    width: theme.spacing(50),
    flex: 1,
    display: "flex",
    flexDirection: "row",
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  imguploader: {
    border: "1px solid",
    background: "#f7f5f5",
    display: "flex",
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  imagelabel: {
    flex: 1,
  },
  actions: {
    flex: 1,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  actionbutton: {
    flex: 1,
    marginRight: "10px",
    maxWidth: "120px",
  },
}));

const EditProduct = ({ getProduct, editProduct, history, product : {
    id,
    name,
    src,
    category,
    sku,
    manufacturer,
    qty,
    modelNumber,
    promotion,
    status,
    desc,
    hasWarranty,
    warrantyType
}, id }) => {
  const classes = useStyles();
  useEffect(() => {
    getProduct(id);
  }, getProduct);
  const [formData, setFormdata] = useState({
    enable: true,
    name: "",
    category: "",
    manufacturer: "",
    model: "",
    sku: "",
    warranty: true,
    warrantytype: "",
    images: "",
  });

  const {
    enable,
    name,
    category,
    manufacturer,
    model,
    sku,
    warranty,
    warrantytype,
    images,
  } = formData;

  const handleOnSubmit = (e) => {
    e.preventDefault();
    addProduct(formData, history);
  };
  const onHandleChange = (e) => {
    e.preventDefault();
    setFormdata({ ...formData, [e.target.name]: e.target.value });
  };
  const handleFileChange = (e) => {};

  return (
    <Fragment>
      <h1>Add Product</h1>
      <form
        className={classes.root}
        onSubmit={(e) => handleOnSubmit(e)}
        autoComplete="off"
      >
        <Paper className={classes.paper}>
          <div className={classes.title}>
            <p>
              <strong>Product General</strong>
              <Divider />
            </p>
          </div>
          <div className={classes.enable}>
            <p>Enable Product</p>
            <FormControlLabel
              control={
                <Switch
                  checked={enable}
                  onChange={(e) => {
                    setFormdata({ ...formData, enable: !enable });
                  }}
                  name="enable"
                  value={enable}
                  color="primary"
                />
              }
              label={enable ? "yes" : "no"}
            />
          </div>
          <div className={classes.name}>
            <TextField
              className={classes.productname}
              required
              name="name"
              value={name}
              onChange={onHandleChange}
              label="product name"
              variant="outlined"
            />
            <FormControl
              required
              variant="filled"
              className={classes.formControl}
            >
              <InputLabel id="demo-simple-select-outlined-label">
                Category
              </InputLabel>
              <Select
                name="category"
                value={category}
                onChange={onHandleChange}
              >
                {categories.map((el, idx) => (
                  <MenuItem value={el.name} key={idx}>
                    {el.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel id="demo-simple-select-outlined-label">
                Manufacturer
              </InputLabel>
              <Select
                name="manufacturer"
                value={manufacturer}
                onChange={onHandleChange}
              >
                {manufacturers.map((el, idx) => (
                  <MenuItem value={el.name} key={idx}>
                    {el.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
          <div className={classes.name}>
            <TextField
              className={classes.modelname}
              required
              name="model"
              value={model}
              label="model name"
              variant="outlined"
              onChange={onHandleChange}
            />
            <TextField
              className={classes.modelname}
              required
              name="sku"
              value={sku}
              label="SKU"
              variant="outlined"
              onChange={onHandleChange}
            />
            <FormControlLabel
              control={
                <Switch
                  checked={warranty}
                  onChange={(e) => {
                    setFormdata({ ...formData, warranty: !warranty });
                  }}
                  name="warranty"
                  value={warranty}
                  color="primary"
                />
              }
              label={warranty ? "Product has warranty" : "No warranty"}
            />
            <FormControl variant="filled" className={classes.formControl}>
              <InputLabel id="demo-simple-select-outlined-label">
                Warranty Type
              </InputLabel>
              <Select
                name="warrantytype"
                value={warrantytype}
                onChange={onHandleChange}
              >
                {warrantyTypes.map((el, idx) => (
                  <MenuItem value={el.name} key={idx}>
                    {el.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>

          <div className={classes.imguploader}>
            <label className={classes.imagelabel} htmlFor="upload-photo">
              <input
                style={{ display: "none" }}
                id="upload-photo"
                name="upload-photo"
                type="file"
                name="images"
                value={images}
                onChange={onHandleChange}
              />
              <Button component="span">
                <img src={imgupload} />
              </Button>
            </label>
          </div>
          <div className={classes.actions}>
            <Button
              className={classes.actionbutton}
              variant="contained"
              color="primary"
              disableRipple
            >
              <Link
                style={{
                  textDecoration: "none",
                  color: "white",
                }}
                to="/"
              >
                Cancel
              </Link>
            </Button>
            <Button
              className={classes.actionbutton}
              variant="contained"
              color="primary"
              disableRipple
              type="submit"
            >
              Submit
            </Button>
          </div>
        </Paper>
      </form>
    </Fragment>
  );
};

EditProduct.propTypes = {
  editProduct: PropTypes.func.isRequired,
  getProduct: PropTypes.func.isRequired,
};

export default connect(null, { editProduct, getProduct })(EditProduct);
