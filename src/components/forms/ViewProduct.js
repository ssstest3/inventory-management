import React, { Fragment, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";

import { getProduct } from "../../actions/products";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: "50px",
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
  },
  item: {
    flex: 1,
  },
}));

const ViewProduct = ({ getProduct, viewProduct, match }) => {
  const classes = useStyles();
  const id = match.params.id;
  useEffect(() => {
    getProduct(id);
  }, [getProduct]);

  return (
    <div className={classes.root}>
      {viewProduct && (
        <div>
          <div className={classes.item}>
            <h1> {viewProduct.name} </h1>
          </div>
          <div className={classes.item}>
            <p> ID : {viewProduct.id} </p>
          </div>
          <div className={classes.item}>
            <p> Model : {viewProduct.modelNumber} </p>
          </div>
          <div className={classes.item}>
            <p> SKU : {viewProduct.sku} </p>
          </div>
          <div className={classes.item}>
            <p> Manufacturer : {viewProduct.manufacturer} </p>
          </div>
          <div className={classes.item}>
            <p> Category : {viewProduct.category} </p>
          </div>
          <div className={classes.item}>
            <p> Warrant Type : {viewProduct.warrantyType} </p>
          </div>
          <div className={classes.item}>
            <p> Status : {viewProduct.status} </p>
          </div>

          {/* <List component="nav" aria-label="secondary mailbox folders">
            <ListItem button>
              <ListItemText primary="Id" secondary={viewProduct.id} />
            </ListItem>
            <ListItem button>
              <ListItemText primary="model" secondary={viewProduct} />
            </ListItem>
            <ListItem button>
              <ListItemText primary="sku" secondary={viewProduct.sku} />
            </ListItem>
            <ListItem button>
              <ListItemText
                primary="warranty type"
                secondary={viewProduct.warrantytype}
              />
            </ListItem>
            <ListItem button>
              <ListItemText primary="enable" secondary={viewProduct.enable} />
            </ListItem>
            <ListItem button>
              <ListItemText primary="category" secondary={viewProduct.category} />
            </ListItem>
            <ListItem button>
              <ListItemText
                primary="manufacturer"
                secondary={viewProduct.manufacturer}
              />
            </ListItem>
          </List> */}
        </div>
      )}
    </div>
  );
};

ViewProduct.propTypes = {
  getProduct: PropTypes.func.isRequired,
  viewProduct: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  viewProduct: state.products.viewProduct,
});

export default connect(mapStateToProps, { getProduct })(ViewProduct);
