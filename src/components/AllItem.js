import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Checkbox from "@material-ui/core/Checkbox";
import { columns } from "../utils/data";
import { getAllProducts } from "../actions/products";
import { connect } from "react-redux";
import ControlledOpenSelect from "./itemAcions";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 440,
  },
});

const AllItem = ({ getAllProducts, products }) => {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  useEffect(() => {
    getAllProducts();
  }, [getAllProducts]);

  const rows =
    products &&
    products.map((product, index) => {
      return {
        "": <Checkbox />,
        ProductID: product.id,
        Thumbnails: <img src="" alt={index} />,
        ProductName: product.name,
        Category: product.category,
        SKU: product.sku,
        Manufacturer: product.manufacturer,
        inStock: product.qty >= 0 ? "yes" : "no",
        QTY: product.qty,
        ModelNumber: product.modelNumber,
        Promotion: product.promotion,
        Status: product.status,
        Actions: <ControlledOpenSelect id={index+1} />,
      };
    });

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <div>
      <div className="listheading">
        <h3>Inventory List</h3>
      </div>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column, idx) => (
                  <TableCell key={idx}>
                    <strong>{column.name}</strong>
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, rid) => {
                  return (
                    <TableRow key={rid}>
                      {columns.map((col, cid) => {
                        const value = row[col.name];
                        return <TableCell key={cid}>{value}</TableCell>;
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 50]}
          component="div"
          count={products && products.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
};

AllItem.propTypes = {
  getAllProducts: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired,
};

const mapStateTopProps = (state) => ({
  products: state.products.products,
});

export default connect(mapStateTopProps, { getAllProducts })(AllItem);
