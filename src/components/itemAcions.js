import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { deleteProduct } from "../actions/products";

const ControlledOpenSelect = ({ deleteProduct, id }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const viewProduct = (e) => {
    e.preventDefault();
    setAnchorEl(null);
  };

  const editProduct = (e) => {
    e.preventDefault();
    setAnchorEl(null);
  };


  return (
    <div>
      <Button
        variant="contained"
        color="primary"
        disableRipple
        onClick={handleMenu}
      >
        Select Option
        <ArrowDropDownIcon />
      </Button>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={open}
        onClose={handleClose}
      >
        <MenuItem >
          <Link
            style={{
              textDecoration: "none",
            }}
            to={`/view-product/${id}`}
          >
            View Product
          </Link>
        </MenuItem>
        <MenuItem onClick={editProduct}>
          <Link
            style={{
              textDecoration: "none",
            }}
            to={`/edit-product/${"sant"}`}
          >
            Edit Product
          </Link>
        </MenuItem>
        <MenuItem onClick={()=>deleteProduct(id)}>Delete Product</MenuItem>
      </Menu>
    </div>
  );
};

ControlledOpenSelect.propTypes = {
  deleteProduct: PropTypes.func.isRequired,
};

export default connect(null, { deleteProduct })(ControlledOpenSelect);
