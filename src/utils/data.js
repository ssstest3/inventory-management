export const columns = [
  {
    name: "",
  },
  {
    name: "ProductID",
  },
  {
    name: "Thumbnails",
  },
  {
    name: "ProductName",
  },
  {
    name: "Category",
  },
  {
    name: "SKU",
  },
  {
    name: "Manufacturer",
  },
  {
    name: "inStock",
  },
  {
    name: "QTY",
  },
  {
    name: "ModelNumber",
  },
  {
    name: "Promotion",
  },
  {
    name: "Status",
  },
  {
    name: "Actions",
  },
];

export const products = [
  {
    id: 1,
    src: "https://source.unsplash.com/2ShvY8Lf6l0/800x599",
    name: "iPhone X",
    category: "Mobile",
    sku: "MH-13",
    manufacturer: "Apple",
    qty: 2,
    modelNumber: "AKC-1",
    promotion: "new",
    status: "enable",
    desc: "",
    hasWarranty: 1,
    warrantyType: "Extended",
  },
  {
    id: 2,
    src: "https://source.unsplash.com/2ShvY8Lf6l0/800x599",
    name: "inverter plus",
    category: "AC",
    sku: "MH-13",
    manufacturer: "Voltas",
    qty: 7,
    modelNumber: "ASDSDFF",
    promotion: "regular",
    status: "draft",
    desc: "",
    hasWarranty: 1,
    warrantyType: "Extended",
  },

  {
    id: 3,
    src: "https://source.unsplash.com/2ShvY8Lf6l0/800x599",
    name: "Jupiter",
    category: "Battery",
    sku: "MH-13",
    manufacturer: "Exide",
    qty: 7,
    modelNumber: "DGFGFD",
    promotion: "special",
    status: "disable",
    desc: "",
    hasWarranty: 1,
    warrantyType: "Extended",
  },

  {
    id: 4,
    src: "https://source.unsplash.com/2ShvY8Lf6l0/800x599",
    name: "OLED",
    category: "TV",
    sku: "MH-13",
    manufacturer: "Sony Bravia",
    qty: 48,
    modelNumber: "SFGDSGSG",
    promotion: "new",
    status: "enable",
    desc: "",
    hasWarranty: 0,
    warrantyType: "Extended",
  },

  {
    id: 5,
    src: "https://source.unsplash.com/2ShvY8Lf6l0/800x599",
    name: "UHD",
    category: "TV",
    sku: "MH-13",
    manufacturer: "Sony Bravia",
    qty: 100,
    modelNumber: "DFYFGH",
    promotion: "regular",
    status: "enable",
    desc: "",
    hasWarranty: 0,
    warrantyType: "Implied",
  },

  {
    id: 6,
    src: "https://source.unsplash.com/2ShvY8Lf6l0/800x599",
    name: "Air plus",
    category: "AC",
    sku: "MH-13",
    manufacturer: "Voltas",
    qty: 95,
    modelNumber: "KJKFF",
    promotion: "special",
    status: "draft",
    desc: "",
    hasWarranty: 1,
    warrantyType: "Implied",
  },

  {
    id: 7,
    src: "https://source.unsplash.com/2ShvY8Lf6l0/800x599",
    name: "iPhone 6",
    category: "Mobile",
    sku: "MH-13",
    manufacturer: "Apple",
    qty: 70,
    modelNumber: "GFGFS",
    promotion: "new",
    status: "disable",
    desc: "",
    hasWarranty: 1,
    warrantyType: "Implied",
  },

  {
    id: 8,
    src: "https://source.unsplash.com/2ShvY8Lf6l0/800x599",
    name: "Saturn",
    category: "Battery",
    sku: "MH-13",
    manufacturer: "Exide",
    qty: 34,
    modelNumber: "HJDFK",
    promotion: "regular",
    status: "enable",
    desc: "",
    hasWarranty: 1,
    warrantyType: "Implied",
  },
];

export const categories = [
  {
    id: 1,
    name: "AC",
  },
  {
    id: 2,
    name: "TV",
  },
  {
    id: 3,
    name: "Battery",
  },
  {
    id: 4,
    name: "Mobile",
  },
];

export const manufacturers = [
  {
    id: 1,
    name: "Voltas",
  },
  {
    id: 2,
    name: "Sony Bravia",
  },
  {
    id: 3,
    name: "Exide",
  },
  {
    id: 4,
    name: "Apple",
  },
];

export const warrantyTypes = [
  {
    id: 1,
    name: "Implied",
  },
  {
    id: 2,
    name: "Extended",
  },
];
