import React, { Fragment } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import MainPage from "./components/MainePage";
import { Provider } from "react-redux";
import AddProduct from "./components/forms/AddProduct";
import ViewProduct from "./components/forms/ViewProduct";
import store from "./store";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Switch>
            <Route exact path="/" component={MainPage} />
            <Route exact path="/add-product" component={AddProduct} />
            <Route exact path="/view-product/:id" component={ViewProduct} />
            {/* <Route exact path="/edit-product/:name" component={MenuAppBar} /> */}
          </Switch>
        </Fragment>
      </Router>
    </Provider>
  );
}

export default App;
