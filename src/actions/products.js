import {
  GET_ALL_PRODUCTS_SUCCESS,
  GET_ALL_PRODUCTS_FAIL,
  GET_PRODUCT_SUCCESS,
  GET_PRODUCT_FAIL,
  ADD_PRODUCT_SUCCESS,
  ADD_PRODUCT_FAIL,
  EDIT_PRODUCT_SUCCESS,
  EDIT_PRODUCT_FAIL,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAIL,
  SEARCH_PRODUCT_SUCCESS,
  SEARCH_PRODUCT_FAIL,
} from "./types";

import { products } from "../utils/data";

export const getAllProducts = () => async (dispacth) => {
  try {
    dispacth({
      type: GET_ALL_PRODUCTS_SUCCESS,
      payload: products,
    });
  } catch (error) {
    dispacth({
      type: GET_ALL_PRODUCTS_FAIL,
    });
  }
};

export const seacrhProduct = ({ searchValue }) => async (dispatch) => {
  let filtereddata = products.filter((product) => {
    return product.name.toLowerCase().indexOf(searchValue) !== -1;
  });

  dispatch({
    type: GET_ALL_PRODUCTS_SUCCESS,
    payload: filtereddata,
  });
};

export const getProduct = (id) => async (dispatch) => {
  try {
    let viewProduct = await products.find((product) => product.id == id);
    dispatch({
      type: GET_PRODUCT_SUCCESS,
      payload: viewProduct,
    });
  } catch (error) {}
};
export const addProduct = (formData, history) => async (dispacth) => {
  const {
    enable,
    name,
    category,
    manufacturer,
    model,
    sku,
    warranty,
    warrantytype,
    images,
  } = formData;

  try {
    let product = {
      id: products.length + 1,
      name: name,
      src: images,
      category: category,
      sku: sku,
      manufacturer: manufacturer,
      qty: 1,
      modelNumber: model,
      promotion: "new",
      status: enable,
      desc: "",
      hasWarranty: warranty,
      warrantyType: warrantytype,
    };

    await products.push(product);

    dispacth({
      type: ADD_PRODUCT_SUCCESS,
    });
    dispacth({
      type: GET_ALL_PRODUCTS_SUCCESS,
      payload: products,
    });

    history.push("/");
  } catch (error) {
    dispacth({
      type: GET_ALL_PRODUCTS_FAIL,
    });
  }
};

export const editProduct = (formData, history) => async (dispacth) => {
  const {
    enable,
    name,
    category,
    manufacturer,
    model,
    sku,
    warranty,
    warrantytype,
    images,
  } = formData;

  try {
    let product = {
      id: products.length + 1,
      name: name,
      src: images,
      category: category,
      sku: sku,
      manufacturer: manufacturer,
      qty: 1,
      modelNumber: model,
      promotion: "new",
      status: enable,
      desc: "",
      hasWarranty: warranty,
      warrantyType: warrantytype,
    };

    await products.push(product);

    dispacth({
      type: GET_ALL_PRODUCTS_SUCCESS,
      payload: products,
    });

    history.push("/");
  } catch (error) {
    dispacth({
      type: GET_ALL_PRODUCTS_FAIL,
    });
  }
};

export const deleteProduct = (id) => async (dispacth) => {
  try {
    let removeIndex = products.map((product) => product.id).indexOf(id);

    // let result = await products.slice(removeIndex, removeIndex + 1);
    var result = [
      ...products.slice(0, removeIndex),
      ...products.slice(removeIndex + 1),
    ];
    dispacth({
      type: DELETE_PRODUCT_SUCCESS,
      payload: result,
    });
  } catch (error) {}
};
